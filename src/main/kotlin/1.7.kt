import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/16
* TITLE: Eleva’l
*/
fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce una base y un exponente:")
    val numero1= scanner.nextInt()
    val numero2 = scanner.nextInt()
    val result = elevadorDeUnNUmeroPorBaseYExponente(numero1, numero2)

    println(result)

}

fun elevadorDeUnNUmeroPorBaseYExponente (numero1:Int, numero2:Int):Int{

    var counter = 1
    for (i in 1 .. numero2){
        counter*= numero1
    }
    return (counter)
}

