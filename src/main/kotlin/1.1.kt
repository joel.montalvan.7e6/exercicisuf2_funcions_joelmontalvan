import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/15
* TITLE: Mida d’un String
*/

fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce una palabra para saber cuantas letras tiene:")
    val palabra = scanner.nextLine()
    val result = contadorDeLetrasDeUnaPalabra(palabra)

    println(result)

}

fun contadorDeLetrasDeUnaPalabra (palabra:String): Int{

    var counterDigits = 0
    for (i in palabra){
        counterDigits++
    }

    return (counterDigits)
}