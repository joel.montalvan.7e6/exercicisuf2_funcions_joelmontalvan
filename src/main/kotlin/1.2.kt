import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/16
* TITLE: String és igual
*/

fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce dos palabras para comprobar si son iguales:")
    val palabra1 = scanner.next()
    val palabra2 = scanner.next()
    val result = palabrasIgualesODiferentes(palabra1, palabra2)

    println(result)

}

fun palabrasIgualesODiferentes (palabra1:String, palabra2:String): Boolean{

    if (palabra1 == palabra2) return true
    else return false
}
