import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/16
* TITLE: Caràcter d’un String
*/
fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce las palabras que quieras y la posicion que quieres imprimir:")
    val palabra = scanner.nextLine()
    val posicionLetra = scanner.nextInt()
    val result = letraEscogidaPorLaPosicion(palabra, posicionLetra)

    println(result)
}

fun letraEscogidaPorLaPosicion(palabra:String, posicionLetra:Int):String{

    if (palabra.length < posicionLetra) return ("La mida de l'String es inferior a $posicionLetra")
    else return ("${palabra[posicionLetra]}")
}
