import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/16
* TITLE: Subseqüència
*/
fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce las palabras que quieras y entre que posiciones quiere imprimirlas:")
    val palabra = scanner.nextLine()
    val start = scanner.nextInt()
    val end = scanner.nextInt()
    val result =letrasImprimidasPorlasPosicionesStartYEnd(palabra, start, end)

    println(result)
}

fun letrasImprimidasPorlasPosicionesStartYEnd (palabra: String, start: Int, end: Int):String {

    var counter = ""
    if (end <= palabra.length && start >= 0){
        for (i in start until  end){
            counter+= palabra[i]
        }
        return (counter)
    }
    else return ("La subseqüència $start - $end de l'String no existe")
}

