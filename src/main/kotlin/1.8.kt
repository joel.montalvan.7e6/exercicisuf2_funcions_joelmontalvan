import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/16
* TITLE: Escriu una línia
*/
fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce el numero de espacios que quieres:")
    val espacio= scanner.nextInt()
    println("Introduce la cantidad de simbolo que quieras imprimir:")
    val numeroDeLetra = scanner.nextInt()
    println("Introduce el simbolo que quieres representar:")
    val letra = scanner.next().uppercase()
    val result = escrituraDeSimbolosAPartirdeEspaciosYRepeticiones(espacio, numeroDeLetra, letra)

    println(result)
}

fun escrituraDeSimbolosAPartirdeEspaciosYRepeticiones (espacio: Int, numeroDeLetra:Int, letra: String): String{

    var counter= ""
    for (i in 1 .. espacio){
        counter+= " "
    }
    for (i in 1 .. numeroDeLetra){
        counter+= letra
    }
    return (counter)
}
