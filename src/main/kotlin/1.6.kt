import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/19
* TITLE: Divideix un String
*/
fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce una palabra:")
    val palabra = scanner.next()
    println("Introduce el caracter que deseas eliminar:")
    val caracter = scanner.next().single()
    val result = divisorDeUnaPalabraQuitandoUnCaracter(palabra, caracter)

    println(result)
}

fun divisorDeUnaPalabraQuitandoUnCaracter (palabra:String, caracter:Char): MutableList<String>{
    val palabrasSeparadas = mutableListOf<String>()
    var espacios = ""
    for (i in palabra) {
        if (i != caracter){
            espacios+= i
        }
        else{
            palabrasSeparadas.add(espacios)
            espacios= ""
        }
    }
    return palabrasSeparadas
}
