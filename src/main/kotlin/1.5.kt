import java.util.Scanner

/*
* AUTHOR: Joel Montalvan Montiel
* DATE: 2022/12/16
* TITLE: D’String a MutableList<Char>
*/
fun main (){

    val scanner = Scanner(System.`in`)
    println("Introduce una palabra:")
    val palabra = scanner.nextLine()
    val result = divisordeLetrasDeUnaPalabra(palabra)

    println(result)
}
fun divisordeLetrasDeUnaPalabra (palabra:String): MutableList<Char>{

    val listaDeLetras = mutableListOf<Char>()
    for (i in palabra){
        listaDeLetras.add(i)
    }
    return listaDeLetras
}

